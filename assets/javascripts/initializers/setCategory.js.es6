export default {
  name: 'categoryLoop',
  initialize() {
    setInterval(function() {
      var categoryElements = document.getElementsByClassName('categoryInstructions');
      var categoryIdElement = document.getElementById('categoryId');
      if(categoryIdElement) {
        var categoryId = "category-" + categoryIdElement.innerHTML;
      }

      for (var i = 0; i < categoryElements.length; i++) {
        if(categoryElements[i].id == categoryId) {
          document.getElementById(categoryId).style.display = "block";
        } else {
          categoryElements[i].style.display = 'none';
        }
      }
    }, 500);
  }
};